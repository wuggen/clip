#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>

#define INIT_BUFSIZE 32

void resize(char **data, char **alloc_end, char **data_end) {
  if (*data_end >= *alloc_end - 1) {
    ptrdiff_t alloc_size = *alloc_end - *data;
    *data = realloc(*data, alloc_size * 2);
    *alloc_end = *data + (alloc_size * 2);
    *data_end = *data + alloc_size - 1;
  }
}

int main(int argc, char **argv) {
  FILE *input = stdin;
  if (argc > 1) {
    input = fopen(argv[1], "r");
    if (!input) {
      char str[128];
      sprintf(str, "Failed to open file %s", argv[1]);
      perror(str);
      exit(EXIT_FAILURE);
    }
  }

  char *data = NULL;
  char *alloc_end = NULL;
  char *data_end = NULL;

  data = malloc(INIT_BUFSIZE * sizeof(char));
  alloc_end = data + INIT_BUFSIZE;
  data_end = data;

  size_t rem, read;
  int newlines = 0;
  do {
    resize(&data, &alloc_end, &data_end);
    rem = (size_t) (alloc_end - data_end - 1);
    read = fread(data_end, 1, rem, input);

    char *i;
    for (i = data_end; i < data_end + read; ++i) {
      if (*i == '\n') ++newlines;
    }

    data_end[read] = '\0';
    data_end += read;
  } while (read == rem);

  if (newlines < 2 && *(data_end - 1) == '\n') {
    *(data_end - 1) = '\0';
    --data_end;
  }

  HGLOBAL data_cp = GlobalAlloc(GMEM_MOVEABLE, (data_end - data + 1) * sizeof(char));
  LPVOID ldata_cp = GlobalLock(data_cp);
  memcpy(ldata_cp, data, (data_end - data + 1) * sizeof(char));
  GlobalUnlock(data_cp);

  OpenClipboard(NULL);
  EmptyClipboard();
  SetClipboardData(CF_TEXT, data_cp);
  CloseClipboard();

  GlobalFree(data_cp);

  free(data);

  if (input != stdin)
    fclose(input);

  return EXIT_SUCCESS;
}
